require 'rails_helper'

RSpec.describe "challenges/index", type: :view do
  before(:each) do
    assign(:challenges, [
      Challenge.create!(
        :name => "Name",
        :description => "MyText",
        :moderation_flag => false,
        :status => "Status",
        :terms => "MyText",
        :creator => nil,
        :requirement => "MyText",
        :payoff => 1,
        :open_flag => false,
        :min_num => 2,
        :max_num => 3
      ),
      Challenge.create!(
        :name => "Name",
        :description => "MyText",
        :moderation_flag => false,
        :status => "Status",
        :terms => "MyText",
        :creator => nil,
        :requirement => "MyText",
        :payoff => 1,
        :open_flag => false,
        :min_num => 2,
        :max_num => 3
      )
    ])
  end

  it "renders a list of challenges" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
