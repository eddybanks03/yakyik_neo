require 'rails_helper'

RSpec.describe "challenges/edit", type: :view do
  before(:each) do
    @challenge = assign(:challenge, Challenge.create!(
      :name => "MyString",
      :description => "MyText",
      :moderation_flag => false,
      :status => "MyString",
      :terms => "MyText",
      :creator => nil,
      :requirement => "MyText",
      :payoff => 1,
      :open_flag => false,
      :min_num => 1,
      :max_num => 1
    ))
  end

  it "renders the edit challenge form" do
    render

    assert_select "form[action=?][method=?]", challenge_path(@challenge), "post" do

      assert_select "input#challenge_name[name=?]", "challenge[name]"

      assert_select "textarea#challenge_description[name=?]", "challenge[description]"

      assert_select "input#challenge_moderation_flag[name=?]", "challenge[moderation_flag]"

      assert_select "input#challenge_status[name=?]", "challenge[status]"

      assert_select "textarea#challenge_terms[name=?]", "challenge[terms]"

      assert_select "input#challenge_creator_id[name=?]", "challenge[creator_id]"

      assert_select "textarea#challenge_requirement[name=?]", "challenge[requirement]"

      assert_select "input#challenge_payoff[name=?]", "challenge[payoff]"

      assert_select "input#challenge_open_flag[name=?]", "challenge[open_flag]"

      assert_select "input#challenge_min_num[name=?]", "challenge[min_num]"

      assert_select "input#challenge_max_num[name=?]", "challenge[max_num]"
    end
  end
end
