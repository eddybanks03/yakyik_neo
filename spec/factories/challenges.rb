FactoryGirl.define do
  factory :challenge do
    name "MyString"
description "MyText"
moderation_flag false
status "MyString"
terms "MyText"
creator nil
requirement "MyText"
deadline "2015-12-04 01:45:53"
payoff 1
open_flag false
min_num 1
max_num 1
  end

end
