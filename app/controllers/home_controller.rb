class HomeController < ApplicationController
  def index
  end

  def about
  end

  def contact
  end

  def faq
  end

  def policy
  end
end
