class Accept < ActiveRecord::Base

  belongs_to :acceptor, polymorphic: true
  belongs_to :accepted, polymorphic: true
  
end
