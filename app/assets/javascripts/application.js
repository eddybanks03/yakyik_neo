//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require jquery.turbolinks
//= require react
//= require react_ujs
//= require components
//= require bootstrap-sprockets
//= require_tree .


var onResize = function() {
  // apply dynamic padding at the top of the body according to the fixed navbar height
  $("body").css("padding-top", $(".nav").height());
};

// attach the function to the window resize event
$(window).resize(onResize);

// call it also when the page is ready after load or reload
$(function() {
  onResize();
});
