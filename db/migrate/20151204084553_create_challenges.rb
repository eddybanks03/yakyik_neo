class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.string :name, null: false
      t.text :description, null: false
      t.boolean :moderation_flag, default: false
      t.string :status, default: 'pending'
      t.text :terms
      t.references :creator, polymorphic: true, index: true
      t.text :requirement
      t.datetime :deadline
      t.integer :payoff
      t.boolean :open_flag, default: true
      t.integer :min_num
      t.integer :max_num

      t.timestamps null: false
    end
  end
end
