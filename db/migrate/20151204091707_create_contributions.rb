class CreateContributions < ActiveRecord::Migration
  def change
    create_table :contributions do |t|
      t.references :contributor, polymorphic: true, index: true
      t.references :contributed, polymorphic: true, index: true
      t.integer :amount
      t.string :charity

      t.timestamps null: false
    end
  end
end
