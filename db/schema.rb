# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151208213157) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accepts", force: :cascade do |t|
    t.integer  "acceptor_id"
    t.string   "acceptor_type"
    t.integer  "accepted_id"
    t.string   "accepted_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "accepts", ["accepted_type", "accepted_id"], name: "index_accepts_on_accepted_type_and_accepted_id", using: :btree
  add_index "accepts", ["acceptor_type", "acceptor_id"], name: "index_accepts_on_acceptor_type_and_acceptor_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "challenge_categories", force: :cascade do |t|
    t.integer  "challenge_id"
    t.integer  "category_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "challenge_categories", ["category_id"], name: "index_challenge_categories_on_category_id", using: :btree
  add_index "challenge_categories", ["challenge_id"], name: "index_challenge_categories_on_challenge_id", using: :btree

  create_table "challenges", force: :cascade do |t|
    t.string   "name",                                null: false
    t.text     "description",                         null: false
    t.boolean  "moderation_flag", default: false
    t.string   "status",          default: "pending"
    t.text     "terms"
    t.integer  "creator_id"
    t.string   "creator_type"
    t.text     "requirement"
    t.datetime "deadline"
    t.integer  "payoff"
    t.boolean  "open_flag",       default: true
    t.integer  "min_num"
    t.integer  "max_num"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "challenges", ["creator_type", "creator_id"], name: "index_challenges_on_creator_type_and_creator_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "challenge_id"
    t.text     "comment"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "comments", ["challenge_id"], name: "index_comments_on_challenge_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "email",      null: false
    t.string   "telephone"
    t.text     "message",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contributions", force: :cascade do |t|
    t.integer  "contributor_id"
    t.string   "contributor_type"
    t.integer  "contributed_id"
    t.string   "contributed_type"
    t.integer  "amount"
    t.string   "charity"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "contributions", ["contributed_type", "contributed_id"], name: "index_contributions_on_contributed_type_and_contributed_id", using: :btree
  add_index "contributions", ["contributor_type", "contributor_id"], name: "index_contributions_on_contributor_type_and_contributor_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name",        null: false
    t.text     "description", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "user_groups", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_groups", ["group_id"], name: "index_user_groups_on_group_id", using: :btree
  add_index "user_groups", ["user_id"], name: "index_user_groups_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "first_name"
    t.string   "last_name"
    t.date     "date_of_birth"
    t.string   "phone_number"
    t.boolean  "admin_flag",             default: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "challenge_categories", "categories"
  add_foreign_key "challenge_categories", "challenges"
  add_foreign_key "comments", "challenges"
  add_foreign_key "comments", "users"
  add_foreign_key "user_groups", "groups"
  add_foreign_key "user_groups", "users"
end
