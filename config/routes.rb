Rails.application.routes.draw do

  root                          'home#index'
  get 'about'               =>  'home#about'
  get 'contact'             =>  'home#contact'
  get 'faq'                 =>  'home#faq'
  get 'policy'              =>  'home#policy'
  # get 'accepts'             =>  'challenges#accepts'

  devise_for :users

resources :challenges do
  member do
    get 'accepts'
  end
  resources :comments
end

  resources :comments
  resources :groups
  resources :charges

end
